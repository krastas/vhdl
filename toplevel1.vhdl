library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL; -- for the signed, unsigned types and arithmetic ops
--loenda nullid
entity func1 is 
    Port ( 	a : in  STD_LOGIC_VECTOR (3 downto 0); --4 bit input
			b : in  STD_LOGIC_VECTOR (3 downto 0); -- 4 bit input 
			o : out  STD_LOGIC_VECTOR (3 downto 0)); --4 bit output 
end func1;

architecture toplevel1 of func1 is
begin

process(a,b)
variable count : unsigned(3 downto 0) := "0000";
begin

count := "0000"; 
for i in 0 to 3 loop
if(a(i) = '0') then
count := count + 1;
end if;
if (b(i) = '0') then
count := count + 1;
end if;
end loop;
o <= std_logic_vector(count);
end process;
	
end toplevel1;