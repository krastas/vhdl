library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library toplevel1, toplevel2, toplevel3, toplevel4, mux1;

entity ALU is --do not change entity, it must match testbench component
    Port ( 	a : in  STD_LOGIC_VECTOR (3 downto 0); --4 bit input
			b : in  STD_LOGIC_VECTOR (3 downto 0); -- 4 bit input 
			op : in STD_LOGIC_VECTOR (1 downto 0); --2 bit input
			o : out  STD_LOGIC_VECTOR (3 downto 0)); --4 bit output 
end ALU;
--An architecture body is used to describe the behavior, data flow, or structure of a design entity.
architecture toplevel of ALU is

--declare components and inner signals here if needed

SIGNAL f1, f2, f3, f4: STD_LOGIC_VECTOR (3 DOWNTO 0);

component func1 is 
    Port ( 	a : in  STD_LOGIC_VECTOR (3 downto 0); --4 bit input
			b : in  STD_LOGIC_VECTOR (3 downto 0); -- 4 bit input 
			o : out  STD_LOGIC_VECTOR (3 downto 0)); --4 bit output 
end component;

component func2 is 
    Port ( 	a : in  STD_LOGIC_VECTOR (3 downto 0); --4 bit input
			o : out  STD_LOGIC_VECTOR (3 downto 0)); --4 bit output 
end component;

component func3 is 
    Port ( 	a : in  STD_LOGIC_VECTOR (3 downto 0); --4 bit input
			b : in  STD_LOGIC_VECTOR (3 downto 0); -- 4 bit input 
			o : out  STD_LOGIC_VECTOR (3 downto 0)); --4 bit output 
end component;

component func4 is 
    Port ( 	a : in  STD_LOGIC_VECTOR (3 downto 0); --4 bit input
			o : out  STD_LOGIC_VECTOR (3 downto 0)); --4 bit output 
end component;

component mux1 is 
    Port ( 	f1 : in  STD_LOGIC_VECTOR (3 downto 0); --4 bit input
			f2 : in  STD_LOGIC_VECTOR (3 downto 0); -- 4 bit input 
			f3 : in  STD_LOGIC_VECTOR (3 downto 0); -- 4 bit input 
			f4 : in  STD_LOGIC_VECTOR (3 downto 0); -- 4 bit input 
			op: in  STD_LOGIC_VECTOR (1 downto 0);
			o : out  STD_LOGIC_VECTOR (3 downto 0)); --4 bit output 
end component;

begin --beginning of the architecture
	F1 : func1 port map (a, b, f1);
	F2 : func2 port map (a, f2);
	F3 : func3 port map (a, b, f3);
	F4 : func4 port map (a, f4);
	M1 : mux1 port map (f1, f2, f3, f4, op, o);
	
end toplevel;