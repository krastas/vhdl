library IEEE;
use IEEE.std_logic_1164.all;
entity mux1 is
	port(
		f1, f2, f3, f4	:	in std_logic_vector(3 downto 0);
		op					:	in std_logic_vector(1 downto 0);
		o					:	out std_logic_vector(3 downto 0));
end mux1;

architecture MUX of mux1 is
begin
MUX: process(f1,f2,f3,f4, op)
begin
	if op="00" then
		o<= f1;
	elsif op="01" then
		o<= f2;
	elsif op="10" then
		o<= f3;
	elsif op="11" then
		o<= f4;
	end if;
	
end process MUX;
end MUX;