LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use ieee.numeric_std.all; --just in case library
use ieee.std_logic_arith.all; --just in case library
 
ENTITY ALU_TB IS --entity of the testbench is always empty
END ALU_TB;

ARCHITECTURE testbench OF ALU_TB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
	component ALU is
	    Port ( 	a : in  STD_LOGIC_VECTOR (3 downto 0); --4 bit input
				b : in  STD_LOGIC_VECTOR (3 downto 0); -- 4 bit input 
				op : in STD_LOGIC_VECTOR (1 downto 0); --1 downto 0
				o : out  STD_LOGIC_VECTOR (3 downto 0)); --4 bit output 
	end component;
    
   --Inputs from the testbench
   signal a_tb : std_logic_vector(3 downto 0);
   signal b_tb : std_logic_vector(3 downto 0);
   signal op_tb : std_logic_vector(1 downto 0);
   signal answer : std_logic_vector(3 downto 0);
   
 	--Output from the testbench
   signal o_tb : std_logic_vector(3 downto 0);


begin
		--component port map
	   uut: ALU port map (
          a => a_tb,
          b => b_tb,
          op => op_tb,
          o => o_tb
        ); 
process
begin

---Test func1
	op_tb <= "00";
	a_tb <= "1000";
	b_tb <= "0011";
	answer <="0101";
	wait for 5 ns;

	a_tb <= "0000";
	b_tb <= "0000";
	answer <="1000";
	wait for 5 ns;
		
	a_tb <= "0110";
	b_tb <= "0111";
	answer <="0011";
	wait for 5 ns;
		
	a_tb <= "1111";
	b_tb <= "1111";
	answer <="0000";
	wait for 5 ns;
	
	--Test func2
	op_tb <= "01";
	a_tb <= "1111";
	answer <="1110";
	wait for 5ns;
	
	a_tb <= "1010";
	answer <="0100";
	wait for 5ns;
	
	a_tb <= "0011";
	answer <="0110";
	wait for 5ns;
	
	a_tb <= "0100";
	answer <="1000";
	wait for 5ns;
	
	--Test func3
	op_tb <= "10";
		
	a_tb <= "1100";
	b_tb <= "0011";
	answer <="1100";
	
	wait for 5ns;
	
	a_tb <= "0000";
	b_tb <= "0000";
	answer <="0001";
	
	wait for 5ns;
	a_tb <= "1111";
	b_tb <= "0001";
	answer <="1111";
	
	wait for 5ns;
	a_tb <= "1010";
	b_tb <= "0010";
	answer <="1110";
	
	wait for 5ns;
	
	--test func4
	op_tb <= "11";
	a_tb <= "0000";
	answer <="0001";
	wait for 5ns;
	
	a_tb <= "1100";
	answer <="0000";
	wait for 5ns;
	
	a_tb <= "1010";
	answer <="0001";
	wait for 5ns;
	
	a_tb <= "0101";
	answer <="0000";
	wait for 5ns;

	
	wait;
end process;

end testbench;