library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
---kaota if, kasuta muutujat

entity func4 is 
    Port ( 	a : in  STD_LOGIC_VECTOR (3 downto 0); --4 bit input
			o : out  STD_LOGIC_VECTOR (3 downto 0)); --4 bit output 
end func4;

architecture toplevel4 of func4 is
begin
process
variable x: std_logic_vector (3 downto 0);
variable y: std_logic_vector (3 downto 0);
variable z: std_logic_vector (3 downto 0);
variable v: std_logic_vector (3 downto 0);
variable w: std_logic_vector (3 downto 0);
variable boo : boolean := true;
begin
x := "0101";
y := "0110";
z := "1100";
v := "1101";
w := "1110";
case boo is 
when (a = x or a = y or a=z or a=v or a=w) => o <= "0000";
when (a /= x and a /= y and a/=z and a/=v and a/=w) => o <= "0001";
end case;


--if (a = "0000" or a = "0001" or a = "0010" or a = "0011" or a = "0100" or a = "0111" or a = "1000" or a = "1001" or a = "1010" or a = "1011")
--then o <= "0001";
--else o <= "0000";

--end if; 

end process;
end toplevel4;