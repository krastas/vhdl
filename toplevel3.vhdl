library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
--b v��rtus m��rab biti a-s, mis muutub 1-ks
entity func3 is 
    Port ( 	a : in  STD_LOGIC_VECTOR (3 downto 0); --4 bit input
			b : in  STD_LOGIC_VECTOR (3 downto 0); -- 4 bit input 
			o : out  STD_LOGIC_VECTOR (3 downto 0)); --4 bit output 
end func3;

architecture toplevel3 of func3 is
begin
process(a, b)
begin

if b = "0000" or b = "0100" or b = "1000" or b = "1100" then
o <= a(3 downto 1) & '1';
	
elsif b = "0001" or b = "0101" or b = "1001" or b = "1101" then
o <= a(3 downto 2) & '1' & a(0);

elsif b = "0010" or b = "0110" or b = "1010" or b = "1110" then
o <= a(3) & '1' & a(1 downto 0);

elsif b = "0011" or b = "0111" or b = "1011" or b = "1111" then
o <= '1' & a(2 downto 0);

end if;
end process;
end toplevel3;