library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.numeric_std.all;
--nihe vasakule
entity func2 is
    Port ( 	a : in  STD_LOGIC_VECTOR (3 downto 0); --4 bit input
			o : out  STD_LOGIC_VECTOR (3 downto 0)); --4 bit output 
end func2;

architecture toplevel2 of func2 is
begin
process (a)

begin
o <= std_logic_vector(shift_left(unsigned(a), 1));
	
end process;	
end toplevel2;